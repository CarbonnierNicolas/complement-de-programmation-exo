package in404.exo61;
import in404.exo61.in404.exo61.client.Client;
import in404.exo61.in404.exo61.serveur.Serveur;
import org.junit.jupiter.api.Test;


public class ServeurClientTest {
    @Test
    public void serveurclient(){
        Client c1 = new Client("bob");
        Client c2 = new Client("patrick");
        Serveur S = new Serveur();

        c1.seConnecter(S);
        c2.seConnecter(S);

        c1.envoyer("Bonjour");
        S.diffuser("Bienvenue");
    }

}

package in404.exo61;

import in404.exo61.in404.exo61.client.Client;
import in404.exo61.in404.exo61.serveur.Serveur;

public class Main {
    public static void main(String[] args){
        Serveur s = new Serveur();
        Client c1 = new Client("Bob");
        Client c2 = new Client("Julie");
        Client c3 = new Client("Marc");

        c1.seConnecter(s);
        c2.seConnecter(s);
        c3.seConnecter(s);

        c3.envoyer("Bonjour");
    }
}

package in404.exo61.in404.exo61.client;

import in404.exo61.in404.exo61.serveur.Serveur;

public class Client {
    String nom;
    Serveur s;

    public Client(String nom){
        this.nom = nom;
    }

    public boolean seConnecter(Serveur S){
        if(S.connecter(this)) {
            this.s = S;
            return true;
        }
        return false;
    }

    public void envoyer(String message){
        if(s != null){
            s.diffuser(message);
        }
        else {
            System.out.println("Non connecter a un serveur");
        }
    }

    public void recevoir(String message){
        System.out.println(this.nom+" : "+ message);
    }

    public String getNom(){
        return this.nom;
    }
}

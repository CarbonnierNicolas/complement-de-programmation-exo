package in404.exo61.in404.exo61.serveur;

import in404.exo61.in404.exo61.client.Client;

import java.util.ArrayList;

public class Serveur {

    ArrayList<Client> listeClient;

    public Serveur(){
        listeClient = new ArrayList<Client>();
    }

    public boolean connecter(Client C){
        for(int i=0;i<listeClient.size();i++){
         if(listeClient.get(i).equals(C)){
             System.out.println("Error, "+listeClient.get(i).getNom()+" already connected");
             return false;
         }
        }
        listeClient.add(C);
        return true;
    }

    public void diffuser(String message){
        System.out.println("Serveur : ");
        for(int i=0;i<listeClient.size();i++){
            listeClient.get(i).recevoir(message);
        }
    }
}

/**
 * Classe ChaineCryptée
 * Cette classe prend une chaine de caractère et un décalage en entrée et permet de cryptée cette chaine de caractère par méthode de décalage.
 * Si la chaine passée en argument est null, on lui passe en argument une chaine vide .
 */
public class ChaineCryptee {
    String chaine;
    int decalage;

    /**
     * Constructeur de la classe, il stocke directement la chaine cryptée et non pas la chaine en claire
     * @param chaine la chaine de caractère en claire
     * @param decalage le décalage
     */
    private ChaineCryptee(String chaine, int decalage){
        if(chaine == null){
            chaine = "";
        }
        this.chaine = chaine;
        this.decalage = decalage;
        this.chaine = crypte();
    }

    public static ChaineCryptee deCryptee(String c, int d){
        ChaineCryptee Ch = new ChaineCryptee(c,26-(d%26));
        ChaineCryptee C = new ChaineCryptee(Ch.chaine,d);
        return C;

    }

    public static ChaineCryptee deEnClair(String c, int d){
        ChaineCryptee C = new ChaineCryptee(c,d);
        return C;
    }

    /**
     * Méthode de décalage de caractère
     * @param c le caractère à décaler
     * @param decalage le décalage
     * @return le caractère décalé/encrypté
     */
    private static char decaleCaractere(char c, int decalage) {
        return (c < 'A' || c > 'Z')? c : (char)(((c - 'A' + decalage) % 26) + 'A');
    }

    /**
     * Méthode pour crypté la chaine de caractère
     * @return la chaine cryptée
     */
    public String crypte() {
        StringBuffer tmp = new StringBuffer();
        String chainecryptee;

        for (int i = 0; i < chaine.length(); i++) {
            tmp.append(decaleCaractere(chaine.charAt(i), this.decalage));
        }

        chainecryptee = String.valueOf(tmp);
        return chainecryptee;
    }

    /**
     * Méthode pour décrypté la chaine de caractère
     * @return la chaine décryptée
     */

   public String decrypte() {
       StringBuffer tmp = new StringBuffer();
       String chainedecryptee;

       for (int i = 0; i < chaine.length(); i++) {
           tmp.append(decaleCaractere(chaine.charAt(i), 26-(this.decalage%26)));
       }
       chainedecryptee = String.valueOf(tmp);
       return chainedecryptee;
   }
}
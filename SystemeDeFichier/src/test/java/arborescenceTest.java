import org.junit.jupiter.api.Test;

public class arborescenceTest {

    @Test
    public void testajout(){
        System.out.println("Test1");

        RegularFile F1 = new RegularFile("patate",1);
        RegularFile F2 = new RegularFile("banane",2);
        Directory R1 = new Directory("Sac");
        Directory R2 = new Directory("Frigo");
        Directory R3 = new Directory("Maison");
        RegularFile F22 = new RegularFile("orange",5);

        R1.addFile(F1);
        R2.addFile(F2);
        R2.addFile(F22);

        R2.addFile(R2);
        R2.addFile(R1);
        R3.addFile(R1);


        System.out.println(R1.getSize());
        System.out.println(R2.getSize());
        System.out.println(R3.getSize());
        System.out.println();
    }
    @Test
    public void testajout2(){
        System.out.println("Test2");

        RegularFile a = new RegularFile("text.txt",22);
        RegularFile a2 = new RegularFile("TD1.txt",12);
        Directory r = new Directory("Cours");
        Directory r2 = new Directory("Info 404");
        Directory r3 = new Directory("Info 607");

        r.addFile(a);
        r2.addFile(a2);
        r3.addFile(a);
        r2.addFile(r3);
        r3.addFile(a2);
        r.addFile(r2);
        r2.addFile(a);

        System.out.println(r2.getSize());
    }
}
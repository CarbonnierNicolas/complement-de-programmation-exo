/**
 * Class for a regular file.
 */
public class RegularFile extends File {
    /**
     * Variable for the size of the regular file.
     */
    private int size;

    /**
     * Constructor of the class.
     * @param name the name of the regular file
     * @param size the size of the regular file
     */
    public RegularFile(String name, int size) {
        super(name);
        this.size = size;
    }

    /**
     * Method that return the size of the regular file.
     * @return the size of the regular file
     */
    public int getSize() {
        return this.size;
    }

    /**
     * Method that check the name of the file.
     * @param n the name of the file
     * @return true because it's a regular file
     */
    public boolean checkName(String n) {
        return true;
    }

}

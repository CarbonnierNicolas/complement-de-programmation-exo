import java.util.ArrayList;

/**
 * Class for a directory.
 */
public class Directory extends File {
    /**
     * List of file in the directory.
     */
    private ArrayList<File> fileList;

    /**
     * Constructor of the class.
     * @param name the name of the directory
     */
    public Directory(String name) {
        super(name);
        fileList = new ArrayList<File>();
    }

    /**
     * Method to add file in the directory.
     * @param f the file to add
     * @return true if the file is added, else otherwise
     */
    public boolean addFile(File f) {
        if (this.name == f.getName()) {
            System.out.println("File " + this.name + " already exixts");
            return false;
        } else {
            for (int i = 0; i < fileList.size(); i++) {
                if (!fileList.get(i).checkName(f.name)) {
                    System.out.println("File " + this.name + " already exixts in");
                    return false;
                }
            }
        }
        if (!f.checkName(name)) {
            System.out.println("File " + this.name + " already exixts");
            return false;
        }
        fileList.add(f);
        return true;
    }

    /**
     * Method that check the name of the file if the file already exist in the tree
     * @param n the name of the file
     * @return true if the file already exist, false otherwise
     */
    protected boolean checkName(String n) {
        if (n == this.name) {
            return false;
        }
        for (int i = 0; i < fileList.size(); i++) {
            if (!fileList.get(i).checkName(n)) {
                return false;
            }
        }
        return true;
    }

    /**
     * Method that calculates the size of the directory.
     * @return the size of the directory
     */
    public int getSize() {
        int t = 0;
        for (int i = 0; i < fileList.size(); i++) {
            t += fileList.get(i).getSize();
        }
        return t;
    }
}

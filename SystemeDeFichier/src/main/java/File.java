/**
 * Abstract class for a file.
 */

public abstract class File {
    /**
     * Name of the file.
     */
    protected String name;

    /**
     * Constuctor of File.
     * @param name The name of the file
     */
    public File(String name) {
        this.name = name;
    }

    /**
     * Method that return the name of the file.
     * @return the name of the file
     */
    public String getName() {
        return this.name;
    }

    /**
     * Method that return the size of the file.
     * @return the size of the file
     */
    public abstract int getSize();

    /**
     * Method that check the name of the file.
     * @param n the name of the file
     * @return true if the file already exist, false otherwise
     */
    protected abstract boolean checkName(String n);
}

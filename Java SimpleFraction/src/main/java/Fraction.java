public class Fraction{
    private int num;
    private int denum;

    public Fraction(int a, int b){
        this.num = a;
        this.denum = b;
    }

    @Override
    public String toString() {
        return ("numerateur : " + num+" denominateur : "+denum);
    }
}
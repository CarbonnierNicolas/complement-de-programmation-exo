import java.lang.Math;

/**
 * This class manages the operation between two operands and calculates the result.
 */
public enum Operation {
    /**
     * The plus operation.
     */
    PLUS("+") {
        @Override
        public float eval(float i, float j) throws ExceptionExceedingMaximum, ExceptionExceedingMinimum {
            float result = 0;

            result = i + j;
            if (Math.abs(result) > MoteurRPN.getMaxValue()) {
                throw new ExceptionExceedingMaximum();
            } else {
                if (Math.abs(result) < MoteurRPN.getMinValue()) {
                    throw new ExceptionExceedingMinimum();
                }
            }

            return result;
        }
    },

    /**
     * The minus operation.
     */
    MOINS("-") {
        @Override
        public float eval(float i, float j) throws ExceptionExceedingMaximum, ExceptionExceedingMinimum {
            float result = 0;

            result = j - i;
            if (Math.abs(result) > MoteurRPN.getMaxValue()) {
                throw new ExceptionExceedingMaximum();
            } else {
                if (Math.abs(result) < MoteurRPN.getMinValue()) {
                    throw new ExceptionExceedingMinimum();
                }
            }

            return result;
        }
    },

    /**
     * The multiply operation.
     */
    MULT("*") {
        @Override
        public float eval(float i, float j) throws ExceptionExceedingMaximum, ExceptionExceedingMinimum {
            float result = 0;

            result = i * j;
            if (Math.abs(result) > MoteurRPN.getMaxValue()) {
                throw new ExceptionExceedingMaximum();
            } else {
                if (Math.abs(result) < MoteurRPN.getMinValue()) {
                    throw new ExceptionExceedingMinimum();
                }
            }

            return result;
        }
    },

    /**
     * The divide operation.
     */
    DIV("/") {
        @Override
        public float eval(float i, float j) throws ExceptionExceedingMaximum, ExceptionExceedingMinimum, ExceptionZeroDivision {
            float result = 0;

            if (i == 0) {
                throw new ExceptionZeroDivision();
            }

            result = j / i;
            if (Math.abs(result) > MoteurRPN.getMaxValue()) {
                throw new ExceptionExceedingMaximum();
            } else {
                if (Math.abs(result) < MoteurRPN.getMinValue()) {
                    throw new ExceptionExceedingMinimum();
                }
            }

            return result;
        }
    };

    /**
     * Symbol of the operation.
     */
    private String symbole;

    /**
     * Constructot of the class.
     * @param symbole of the operation
     */
    Operation(String symbole) {
        this.symbole = symbole;
    }

    /**
     * Method to evaluate and return the result of an operation.
     * This method is overrided in each operation
     * @param i the first operand
     * @param j the second operand
     * @return the result of the operation
     * @throws ExceptionExceedingMinimum if an operand or a result exceed the min value
     * @throws ExceptionExceedingMaximum if an operand or a result exceed the max value
     * @throws ExceptionZeroDivision if the denominator of a division is zero
     */
    public abstract float eval(float i, float j) throws ExceptionExceedingMinimum, ExceptionExceedingMaximum, ExceptionZeroDivision;

}

public class ExceptionZeroDivision extends Exception {
    public ExceptionZeroDivision(){
        super("Division by zero is impossible");
    }
}

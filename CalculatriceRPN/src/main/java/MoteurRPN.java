import java.util.Stack;
import java.lang.Math;

/**
 * This class manages the operands stack.
 */
public class MoteurRPN {
    /**
     * The maximum value that we can't exceed.
     */
    private static final float MAX_VALUE = (float) 999.0;

    /**
     * The minimum value that we can't exceed.
     */
    private static final float MIN_VALUE = (float) 0.0;

    /**
     * The operand stack that contains the operands.
     */
    private  Stack<Float> operandeStack;

    /**
     * Constructor of the class.
     */
    public MoteurRPN() {
        operandeStack = new Stack<Float>();
    }

    /**
     * Method for adding an operand to the stack.
     * @param operand the new operand
     * @throws ExceptionExceedingMaximum if the operand exceeds the maximum
     * @throws ExceptionExceedingMinimum if the operand exceeds the minimum
     */
    public void addOperand(float operand)throws ExceptionExceedingMaximum, ExceptionExceedingMinimum {
        if (Math.abs(operand) >= MoteurRPN.MAX_VALUE) {
            System.out.println(operand);
            throw new ExceptionExceedingMaximum();
        } else {
            if (Math.abs(operand) <= MoteurRPN.MIN_VALUE) {
                throw new ExceptionExceedingMinimum();
            }
        }
        operandeStack.push(operand);
    }

    /**
     * Method that calculates the operation between two operands
     * @param operation the type of the operation, plus; minus, divide or multiply
     * @throws ExceptionMissToken if it is missing an operand
     * @throws ExceptionExceedingMaximum if the result of the operation exceeds the max value
     * @throws ExceptionExceedingMinimum if the result of the operation exceeds the min value
     * @throws ExceptionZeroDivision if the denominator of the division is zero
     */
    public void operation(Operation operation) throws ExceptionMissToken, ExceptionExceedingMaximum, ExceptionExceedingMinimum, ExceptionZeroDivision {
        float operand1 = 0, operand2 = 0;
        if (operandeStack.isEmpty()) {
            throw new ExceptionMissToken();
        } else {
            operand1 = operandeStack.pop();
            if (operandeStack.isEmpty()) {
                throw new ExceptionMissToken();
            } else {
                operand2 = operandeStack.pop();
            }
        }
        operandeStack.push(operation.eval(operand1, operand2));
    }

    /**
     * Method to get the max value.
     * @return the max value
     */
    public static float getMaxValue() {
        return MAX_VALUE;
    }

    /**
     * Method to get the min value.
     * @return the min value
     */
    public static float getMinValue() {
        return MIN_VALUE;
    }

    /**
     * Method to get the operand stack.
     * @return the operand stack
     */
    public Stack<Float> getOperandeStack() {
        return operandeStack;
    }

    /**
     * Method to get the operand stack in string format.
     * @return the operand stack in String format
     */
    @Override
    public String toString() {
        return operandeStack.toString();
    }
}

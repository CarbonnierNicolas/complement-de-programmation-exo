public enum CalculatriceRPN {

    /**
     * Enumeration of main.
     */
    MAIN;

    /**
     * Method to run the calcutriceRPN.
     */
    private static void calculatriceRPN() {
        SaisieRPN s; {
            try {
                s = new SaisieRPN();
            } catch (ExceptionMissToken exceptionMissToken) {
                exceptionMissToken.printStackTrace();
            } catch (ExceptionExceedingMaximum exceptionExceedingMaximum) {
                exceptionExceedingMaximum.printStackTrace();
            } catch (ExceptionExceedingMinimum exceptionExceedingMinimum) {
                exceptionExceedingMinimum.printStackTrace();
            } catch (ExceptionZeroDivision exceptionZeroDivision) {
                exceptionZeroDivision.printStackTrace();
            }
        }
    };

    /**
     * Method to run the main programm.
     * @param args
     */
    public static void main(String args[]) {
        MAIN.calculatriceRPN();
    }
}


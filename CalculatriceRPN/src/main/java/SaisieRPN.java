import java.util.Scanner;

/**
 * This class manages what the user is typing.
 */
public class SaisieRPN {

    /**
     * The method to manages what the user typed.
     * @throws ExceptionMissToken if it is missing an operand
     * @throws ExceptionExceedingMaximum if an operand or a result exceed the max value
     * @throws ExceptionExceedingMinimum if an operand or a result exceed the min value
     * @throws ExceptionZeroDivision if the denominator of a division is zero
     */
    SaisieRPN() throws ExceptionMissToken, ExceptionExceedingMaximum, ExceptionExceedingMinimum, ExceptionZeroDivision {
        System.out.println("Veuillez saisir une Opération :");
        boolean exit = false;
        MoteurRPN calc = new MoteurRPN();
        StringBuilder affichage = new StringBuilder();
        while (!exit) {
            Scanner sc = new Scanner(System.in);
            String str = sc.nextLine();
            String sep[] = str.split(" ");
            affichage.append(str + " ");
            if (sep[0].equals("+") || sep[0].equals("-") || sep[0].equals("*") || sep[0].equals("/")) {
                if (sep[0].equals("+")) {
                    Operation o = Operation.PLUS;
                    calc.operation(o);
                    System.out.println(calc);
                }
                if (sep[0].equals("-")) {
                    Operation o = Operation.MOINS;
                    calc.operation(o);
                    System.out.println(calc);
                }
                if (sep[0].equals("*")) {
                    Operation o = Operation.MULT;
                    calc.operation(o);
                    System.out.println(calc);
                }
                if (sep[0].equals("/")) {
                    Operation o = Operation.DIV;
                    calc.operation(o);
                    System.out.println(calc);
                }
                System.out.println(affichage);

            } else {
                if (sep[0].equals("exit") || sep[0].equals("EXIT")) {
                    if (calc.getOperandeStack().isEmpty()) {
                        exit = true;
                    } else {
                        float result = calc.getOperandeStack().pop();
                        if (calc.getOperandeStack().isEmpty()) {
                            exit = true;
                            System.out.println(String.format("%20s", result));
                    } else {
                            throw new ExceptionMissToken();
                        }
                    }
                } else {
                    calc.addOperand(Float.parseFloat(sep[0]));
                    System.out.println(affichage);
                }
            }
        }
    }
}

import org.junit.jupiter.api.Test;

import java.util.Stack;

import static org.junit.jupiter.api.Assertions.*;

class SaisieRPNTest {

    @Test
    public void testExcedingMinimum() throws ExceptionExceedingMinimum, ExceptionExceedingMaximum {
        MoteurRPN test = new MoteurRPN();
        int number = 0;
        try {
            test.addOperand(number);
            fail("Erreur : testExcedingMinimum");
        } catch(ExceptionExceedingMinimum aExp) {
            assert(true);
            System.out.println("Test testExceedingMinimum reussi " + number+ "inferieur ou égale au minimum");
        }
    }

    @Test
    public void testExcedingMaximum() throws ExceptionExceedingMinimum, ExceptionExceedingMaximum {
        MoteurRPN test = new MoteurRPN();
        int number = 1000;
        try {
            test.addOperand(number);
            fail("Erreur : testExcedingMaximum");
        }catch(ExceptionExceedingMaximum aExp){
            assert(true);
            System.out.println("Test testExcedingMinimum reussi " + number+ "superieur ou égale au maximum");
        }
    }

    @Test
    public void testDivideByZero() throws ExceptionExceedingMinimum, ExceptionExceedingMaximum, ExceptionMissToken, ExceptionZeroDivision {
        //Si TestExedingMinimum verifier Impossible car 0 est le minimum
        MoteurRPN test = new MoteurRPN();
        Operation O = Operation.DIV;
        float number1 = 2;
        float number2 = 0;

        test.addOperand(number1);
        try {
            test.addOperand(number2);
            fail("Erreur : testDivideByZero");
            try {
                test.operation(O);
                fail("Erreur : testDivideByZero");
            }catch(ExceptionExceedingMaximum aExp){
                assert(true);
                System.out.println("Test testDivideByZero reussi Division par zéro impossible");
            }
        }catch(ExceptionExceedingMinimum aExp){
            assert(true);
            System.out.println("Test testDivideByZero reussi Division par zéro impossible");
        }

    }

    @Test
    public void testDivide() throws ExceptionExceedingMinimum, ExceptionExceedingMaximum, ExceptionMissToken, ExceptionZeroDivision {
        MoteurRPN test=new MoteurRPN();
        Operation O = Operation.DIV;
        float number1 = 2;
        float number2 = 3;

        test.addOperand(number1);
        test.addOperand(number2);
        test.operation(O);

        Stack<Float> result = test.getOperandeStack();
        if(result.pop() == number1/number2){
            assert(true);
        }
        else {
            assert(false);
        }
    }

    @Test
    public void testMultiplication() throws ExceptionExceedingMinimum, ExceptionExceedingMaximum, ExceptionMissToken, ExceptionZeroDivision {
        MoteurRPN test=new MoteurRPN();
        Operation O = Operation.MULT;
        float number1 = 2;
        float number2 = 3;

        test.addOperand(number1);
        test.addOperand(number2);
        test.operation(O);

        Stack<Float> result = test.getOperandeStack();
        if(result.pop() == number1*number2){
            assert(true);
        }
        else {
            assert(false);
        }
    }

    @Test
    public void testSubstraction() throws ExceptionExceedingMinimum, ExceptionExceedingMaximum, ExceptionMissToken, ExceptionZeroDivision {
        MoteurRPN test=new MoteurRPN();
        Operation O = Operation.MOINS;
        float number1 = 2;
        float number2 = 3;

        test.addOperand(number1);
        test.addOperand(number2);
        test.operation(O);

        Stack<Float> result = test.getOperandeStack();
        if(result.pop() == number1-number2){
            assert(true);
        }
        else {
            assert(false);
        }
    }

    @Test
    public void testAddition() throws ExceptionExceedingMinimum, ExceptionExceedingMaximum, ExceptionMissToken, ExceptionZeroDivision {
        MoteurRPN test=new MoteurRPN();
        Operation O = Operation.PLUS;
        float number1 = 2;
        float number2 = 3;

        test.addOperand(number1);
        test.addOperand(number2);
        test.operation(O);

        Stack<Float> result = test.getOperandeStack();
        if(result.pop() == number1+number2){
            assert(true);
        }
        else {
            assert(false);
        }
    }

    @Test
    public void testMissingToken() throws ExceptionExceedingMinimum, ExceptionExceedingMaximum, ExceptionMissToken, ExceptionZeroDivision {
        MoteurRPN test=new MoteurRPN();
        Operation O = Operation.PLUS;
        float number1 = 2;

        test.addOperand(number1);

        try {
            test.operation(O);
            fail("Erreur : testMissingToken");
        }catch(ExceptionMissToken  aExp){
            assert(true);
        }
    }
}
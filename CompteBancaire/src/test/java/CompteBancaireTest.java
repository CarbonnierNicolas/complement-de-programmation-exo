import org.junit.jupiter.api.Test;

public class CompteBancaireTest {

    @Test
    public void testInitCompte(){
        try{
            CompteBancaire compte = new CompteBancaire(-10);
        }
        catch(ExceptionSoldeNegatif e){};
    }

    @Test
    public void testCredit(){
        try {
            CompteBancaire compte = new CompteBancaire(1000);
            compte.Credit(-100);
        }
        catch (ExceptionSoldeNegatif e){}
        catch (ExceptionMontantNegatif e){};
    }

    @Test
    public void testDebit() {
        try {
            CompteBancaire compte = new CompteBancaire(500);
            compte.Debit(-600);
        }
        catch (ExceptionSoldeNegatif e){}
        catch (ExceptionSoldeInsuffisant e){}
        catch (ExceptionMontantNegatif e){};
    }

    @Test
    public void testVirement(){
        try {
            CompteBancaire compte1 = new CompteBancaire(1000);
            CompteBancaire compte2 = new CompteBancaire(100);
            compte1.Virement(compte2, 1500);
        }
        catch (ExceptionSoldeNegatif e){}
        catch (ExceptionMontantNegatif e){}
        catch (ExceptionSoldeInsuffisant e){};
    }
}
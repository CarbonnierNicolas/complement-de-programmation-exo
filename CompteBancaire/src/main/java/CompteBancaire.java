/**
 * Classe permettant de creer et gerer un compte bancaire.
 *
 */

public class CompteBancaire {

    /**
     * Variable pour stocker et gerer le solde su compte
     * */

    private int Solde;

    /**
     * Constructeur de la classe compte
     *
     * @param solde
     * @throws ExceptionMontantNegatif expception lancee si le montant passé en parametre et negatif
     *
     * On ne peut pas creer un compte avec un solde negatif
     * */

    CompteBancaire(int solde) throws ExceptionSoldeNegatif{
        if (solde < 0) {
            throw new ExceptionSoldeNegatif();
        } else {
            this.Solde = solde;
        }
    }

    /**
     * Methode permettant de consulter le solde du compte
     * */

    public int Consultation(){
        return this.Solde;
    }

    /**
     * Methode permettant de crediter un montant sur le compte
     *
     * @param credit
     * @throws ExceptionMontantNegatif
     *
     * On ne peut pas crediter un montant negatif sur le compte
     * */

    public void Credit(int credit) throws ExceptionMontantNegatif{
        if(credit < 0){
            throw new ExceptionMontantNegatif();
        } else {
            this.Solde = this.Solde + credit;
        }
    }

    /**
     * Methode permettant de debiter un montant sur le compte
     *
     * @param debit
     * @throws ExceptionMontantNegatif
     * @throws ExceptionSoldeInsuffisant exception lancee si le montant du debit est superieur au solde du compte
     *
     * On ne peut pas debiter un montant negatif et l'on ne peut pas debiter si le solde du compte et inferieur au montant du debit
     * */

    public void Debit(int debit) throws ExceptionSoldeInsuffisant,ExceptionMontantNegatif{
        if(debit<0){
            throw new ExceptionMontantNegatif();
        }
        if(this.Solde<debit){
            throw new ExceptionSoldeInsuffisant();
        }
        else{
            this.Solde = this.Solde - debit;
        }
    }

    /**
     * Methode permettant d'effectuer un virement vers un autre compte
     *
     * @param compte compte bancaire qui recoit le virement
     * @param montant
     *
     * @throws ExceptionSoldeInsuffisant
     * @throws ExceptionMontantNegatif
     */

    public CompteBancaire Virement(CompteBancaire compte, int montant) throws ExceptionMontantNegatif,ExceptionSoldeInsuffisant{
        if(montant<0){
            throw new ExceptionMontantNegatif();
        }
        if(this.Solde<montant) {
            throw new ExceptionSoldeInsuffisant();
        }
        compte.Credit(montant);
        this.Solde = this.Solde - montant;
        return compte;
    }
}
